 Quelques scripts pour gérer les notes.

  - csv vers pronote : comme son nom l'indique. Utilise xdotool.
  - plickers : pour ceux qui utilisent ce sytème de QCM, on trouvera un script pour traiter les csv récupérés, en faire une note, et l'envoyer vers pronote, ainsi qu'un script pour peupler une classe en début d'année.
  - lecture note : si vous avez noté dans un tableur les points obtenus par les élève à chaque question d'un devoir, ceci calcule une moyenne et dresse un petit bilan pour chaque élève.