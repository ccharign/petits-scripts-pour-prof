#! /usr/bin/python3
# -*- coding: utf-8 -*-

#Necessite le paquet linux xdotool.

import os
import time
import subprocess
import sys

INFO="/home/moi/enseignement/Informatique/"

def appui(s):
    """simule l'appui sur la touche s."""
    subprocess.call(["xdotool", "type", s])

def lecture (chemin,sep=";"):
    """lit le tableau de notes. Format csv, sep indique le séparateur de champs."""
    fichier=open(chemin,'r')
    notes=[]
    for ligne in fichier:
        notes.append(ligne.strip().split(sep))
    return(notes)
    

def cestParti(chemin,cols,sep=";"):
    """cols est la liste des colonnes à rentrer dans pronote."""
    notes=lecture(chemin,sep)
    for nCol in cols:
        print(" Vous avez 5s pour cliquer sur le haut de la colonne")
        time.sleep(5)
        for ligne in notes:
            for car in ligne[nCol]:
                appui(car)
            appui("\r\n")




if __name__=="__main__":
    if len(sys.argv)==1:
        print("""
    Utilisation : csv_vers_pronote.py chemin_du_csv col_1 col_2 ... col_n.
        """)
    else:
        chemin=sys.argv[1]
        cols=[int(x) for x in sys.argv[2:]]
        cestParti(chemin,cols)

else:
    #Affichage de l'aide:
    print("""

    Pour lancer le programme, taper:
     "cestParti( chemin, [col1,col2,...])", en remplacant:
     - chemin par l'adresse du fichier csv (entre guillemets, y compris le ".csv" )
     - col1,col2,... par les numeros des colonnes de ce fichier que vous voulez rentrer sous pronote.

     Par défaut, les valeurs dans le fichier csv sont séparées par des points-virgule. Pour le changer préciser "sep=...". Par exemple : 
     cestParti( chemin, [col1,col2,...],sep=",")
     
     Attention, le fichier csv ne doit contenir aucune ligne en trop ni au debut, ni a la fin.

     """
    )



