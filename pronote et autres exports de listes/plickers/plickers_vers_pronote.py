#! /usr/bin/python3
# -*- coding: utf-8 -*-

INFO="/home/moi/enseignement/Informatique"
chemin801=INFO+"/IPT1A/plickersS1.csv"
cheminOptionMP=INFO+"/option MP/plickers.csv"
cheminMPPasOption=INFO+"/IPT 2A/MP/plickers.csv"


# A FAIRE :
#       - voir la gestion d'options mutuellement incompatibles dans argparse

import time
import subprocess
import argparse 

def ajouteDansDico(x,d,cle):
    """ajoute x dans le dictionnaire d à l'entrée cle, s'il n'y est pas déjà. Il doit s'agir d'un dictionnaire de listes."""
    if cle in d.keys():
        if not x in d[cle]:
            d[cle].append(x)
    else:
        d[cle]=[x]

def afficheDico(d):
    """ Affiche dans l'ordre alphabétique des clefs"""
    for c in sorted(d.keys()):
        print (c," : ",d[c])


def prefixe(p,m):
    """regarde si p est un préfixe de m."""

    l=len(p)
    lm=len(m)
    if l>lm:
        return(False)
    
    for i in range(l):
        if p[i]!=m[i]:
            return(False)
    return(True)

def enleve_guillemets(c):
    return c[1:-1]
    
def pourcent_vers_int(c):
    """ c est une chaîne dont le dernier caractère est un %."""
    
    res=c[:-1]
    try :
        print(res)
        return int(res)
    except:
        return None
    

def echangeNomPrenom(c):
    """ c est de la forme "Prenom Nom", ceci renvoie alors "Nom Prenom".
    Fonctionne à condition qu'il n'y ait qu'un seul espace."""
    l=c.split(' ')
    assert len(l)==2
    return (l[1]+" "+l[0])


def extrait(chemin):
    """ Renvoi un dictionnaire tel que notes[nom]=liste des notes."""
    entree=open(chemin,'r')

    notes={}

    def litBloc():
        ligne=entree.readline()
        noms=[]
        while ligne!="":
            donnees=[enleve_guillemets(x) for x in ligne.strip().split(',')]
            #print(donnees)
            note=pourcent_vers_int(donnees[3])
            if not prefixe("Guest", donnees[1]):
                ajouteDansDico(note, notes,  donnees[2]+" "+donnees[1] )
            ligne=entree.readline().strip()

    
    ligne=entree.readline() #normalement, c'est le mois (septembre a priori)
    print(ligne)
    while ligne!="":
        litBloc()
        ligne=entree.readline().strip() # on lit le mois
        print(ligne)
    entree.close()
    
    return notes


def moyennes(notes):
    res={}
    for nom in notes.keys() :
        vraies_notes = [ n for n in notes[nom] if not n is None]
        nn=len(vraies_notes)
        if nn>=1:
            res[nom]= sum( vraies_notes)/(5*nn)
        else : res[nom]="nn"
    return res




def affiche(chemin):
    dicoMoyennes=moyennes(extrait(chemin))
    afficheDico(dicoMoyennes)
    

def note_vers_str(n):
    """ Enlève les virgules et transforme en chaîne."""
    return str(n)

def appui(s):
    """simule l'appui sur la touche s."""
    subprocess.call(["xdotool", "type", s])


def envoi(chemin):
    """ Simule l'appui des touches, pour rentrer les notes dans pronote. Attention : ils faut qu'elles soient dans le bon ordre..."""
    tabMoyennes=moyennes(extrait(chemin))
    print("vous avez 5s pour cliquer sur le haut de la colonne")
    time.sleep(5)
    for nom in sorted(tabMoyennes.keys()):
            for car in note_vers_str(tabMoyennes[nom]):
                appui(car)
            appui("\r\n")

### Partie parseur ###

def récupère_arguments():
    parseur=argparse.ArgumentParser()
    
    parseur.add_argument("chemin", help="fichier csv à lire", type=str)
    parseur.add_argument("-a",  "--affiche", help="affiche les notes", action="store_true", default=False)
    
    parseur.add_argument("-e",  "--export", help="Simule l'appui de touches du claviers pour écrire les notes", action="store_true", default=False)
    
    return parseur.parse_args()


if __name__=="__main__":
    args=récupère_arguments()
    print (args.__dict__)
    
    chemin=args.chemin
    if args.affiche:
        affiche(args.chemin)
    if args.export:
        envoi(args.chemin)
        
