#! /usr/bin/python3
# -*- coding: utf-8 -*-

#Necessite le paquet linux xdotool.

import os
import time
import subprocess
import sys
sys.path.append("/home/moi/Dropbox/programmation")
import boiteAOutils as bo

def appui(s):
    """simule l'appui sur la touche s."""
    subprocess.call(["xdotool", "type", s])


chemin802="/home/moi/Dropbox/informatique/IPT 1A/802/listeEleves.csv"
chemin801="/home/moi/Dropbox/informatique/IPT 1A/801/listeEleves.csv"
chemin902="/home/moi/Dropbox/informatique/IPT 2A/PC*/listeEleves.csv"
chemin903="/home/moi/Dropbox/informatique/option MP/MP.csv"

def lecture(chemin,debug=False, sep=";"):
    """ Récupère la liste des élèves, sous la forme "prenom Initiale_nom".
    Fichier csv, format : "nom; prénom", pas de première ligne avec les noms de colonnes. """
    entree=open(chemin,"r", encoding="utf8")
    listeEleves=[]
    for ligne in entree:
        print(ligne)
        if debug:print(ligne)
        [nom,prenom]=ligne.strip().split(sep)
        listeEleves.append( prenom+" "+nom[0])
    entree.close()
    return listeEleves

def envoi(chemin,debug=False, sep=";"):
    listeEleves=lecture(chemin,debug, sep)
    print("vous avez 5s pour cliquer sur la case à remplir")
    time.sleep(5)
    for nom in listeEleves:
        for car in nom:
            appui(car)
        appui("\r\n")


def lectureSelonOption(chemin, option=["SI",""], debug=False, sep=";"):
    dico={}
    for o in option:
        dico[o]=[]
    entree=open(chemin,"r", encoding="utf8")
    for ligne in entree:
        print(ligne)
        if debug:print(ligne)
        [nom,prenom,option]=ligne.strip().split(sep)
        dico[option].append( prenom+" "+nom[0])
    return dico


def envoiSelonOption(chemin, option=["SI",""], debug=False, sep=";"):
    """ Prend un fichier csv où les entrées sont de la forme "Nom; Prénom; Option", et permet de remplir un groupe par option."""
    dicoEleves=lectureSelonOption(chemin,option, debug, sep)
    bo.afficheDico(dicoEleves)
    for o in dicoEleves.keys():
        print("Option {}.\n")
        print( e+"\n" for e in dicoEleves[o])
        print("Vous avez 15s pour cliquer sur la case à remplir.\n Il y a {} élèves dans ce groupe.".format(len(dicoEleves[o])))
        time.sleep(15)
        for nom in dicoEleves[o]:
            for car in nom:
                appui(car)
            appui("\r\n")

