#! /usr/bin/python3
# -*- coding: utf-8 -*-


# Lit un fichier csv contenant les notes à chaque question d'un devoir.


import numpy as np
import sys
import os

def mon_float(c):
    if c=="": return None
    else: return float(c.replace(",", "."))

def mon_str(n):
    """ Transforme en chaîne puis remplace les . par des , """
    return str(n).replace(".", ",")

    
def bouche_trous(ligne):
    """
    Remplace tous les "" dans ligne par le dernier élément ≠ "".
    Sert pour les numéros des questions.
    """
    q=""
    for i,x in enumerate(ligne):
        if x != "":
            q=x
        else:
            ligne[i]=q
    return ligne


def ligne_vide(l):
    """ Indique si l ne contient que des ""
    """
    res=True
    for x in l:
        res = res and x==""
    return res


def extraitDonnées(chemin):
    """
    Entrée : adresse du fichier csv.
    Effet : - crée un dossier CR (compte rendu) avec les erreurs de chaque élève
            - crée un fichier notes.csv contenant les notes de chaque élève.
    """
    
    entrée=open(chemin)
    sortie=open("notesClasse.csv","w")
    
    os.makedirs("CR",exist_ok=True)
    
    def ligne_suivante():
        return entrée.readline().strip().split(";")
    
    #lignes sautées
    ligne=entrée.readline().strip().split(";")
    while ligne_vide(ligne):
        ligne=ligne_suivante()

    #Les lignes suivantes sont les num de question
    num_questions=[]
    while not ligne_vide(ligne):
        num_questions.append( bouche_trous( ligne[1:] ))
        ligne=ligne_suivante()
    #print(num_questions)
    
    # À ce stade, ligne contient la ligne vide avant le barème
    while ligne_vide(ligne):
        ligne=ligne_suivante()
    barème=list(map(mon_float, ligne[1:]))
    for i,n in enumerate(barème):
        if n ==None: barème[i]=0
    tot=sum(barème)
    #print(barème)
    print("Nombre total de points :", tot)



    for ligne in entrée:
        tmp=ligne.strip().split(";")
        nom=tmp[0]
        notes=list(map(mon_float, tmp[1:]))
        note=CR_élève(barème, notes, nom, num_questions)
        sortie.write( f"{nom};{mon_str(note)}\n")
    entrée.close()


def concat(l):
    """Concatène une liste de chaînes avec des espaces."""
    return " : ".join([c for c in l if c!=""])


def CR_élève(barème, notes, nom, num_questions):
    """
    Entrée : barème, de type list, contient le nb de points de chaque question
             nom de l'élève
             num_question de type list list contient la description des questions (utilisé dans le compte rendu pour dire à l'élève où sont ses erreurs.
             notes : tableau de toutes les notes de la classe.
    Sortie : la note de l'élève
    Effet : crée le compte rendu de l'élève dans le dossier CR.
    """
    def question(i):
        """ Renvoie le numéro de la question"""
        return concat( [q[i] for q in num_questions])

    n=len(barème)
    tot=sum(barème)
    
    sortie=open("CR/"+nom+".txt","w")
    traitées=[i for i in range(n) if notes[i]!=None]
    note= sum( barème[i]*notes[i]/4 for i in traitées )
    sortie.write("Note : "+str(note)+" / " + str(tot) + "\n")
    print(nom, note/tot*20)
    
    presque=[question(i)+"\n" for i in traitées if 3<=notes[i]<4 ]
    moyen=[question(i)+"\n" for i in traitées if 2<=notes[i]<3 ]
    bof=[question(i)+"\n" for i in traitées if notes[i]<2 ]
    
    sortie.write("\n\nPetite(s) bêtise(s) à corriger:\n\n")
    sortie.writelines(presque)
    sortie.write("\n\nL'idée est là, mais ça reste à concrétiser :\n\n")
    sortie.writelines(moyen)
    sortie.write("\n\nÀ corriger :\n\n")
    sortie.writelines(bof)

    sortie.close()
    return note/tot*20




if __name__=="__main__":
    chemin=sys.argv[1]
    extraitDonnées(chemin)
