#!/usr/bin/python3
# -*- coding:utf-8 -*-


import sys
codefile = sys.argv[1]
delim1 = sys.argv[2].replace("\\", "")  # Enlever les \ qui venaient de l’échappement Latex
delim2 = sys.argv[3].replace("\\", "")
f = open(codefile)
code = f.read()
f.close()
f = open(codefile + '.snippet', 'w')

try:
    f.write(code.split(delim1, 1)[1].split(delim2, 1)[0])
except Exception as e:
    f.write(f'Erreur dans minted_delim :\n {e}')
f.close()
