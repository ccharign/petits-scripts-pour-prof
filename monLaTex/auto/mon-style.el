(TeX-add-style-hook
 "mon-style"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "french") ("hyperref" "pdfstartview=FitH" "colorlinks" "linkcolor=darkgray") ("grffile" "space") ("tcolorbox" "most") ("piton" "escape-inside=$$") ("algorithm2e" "french" "linesnumbered")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-environments-local "python")
   (add-to-list 'LaTeX-verbatim-environments-local "sql")
   (add-to-list 'LaTeX-verbatim-environments-local "html")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb*")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "microtype"
    "article"
    "art10"
    "shellesc"
    "lmodern"
    "babel"
    "xspace"
    "changepage"
    "geometry"
    "graphicx"
    "fancyhdr"
    "multicol"
    "lscape"
    "hyperref"
    "siunitx"
    "xcolor"
    "footnote"
    "subcaption"
    "float"
    "enumitem"
    "grffile"
    "witharrows"
    "tikz"
    "tcolorbox"
    "listings"
    "minted"
    "piton"
    "stmaryrd"
    "diagbox"
    "csquotes"
    "algorithm2e"
    "answers"
    "lastpage"
    "ifthen"
    "unicode-math"
    "expl3")
   (TeX-add-symbols
    '("exo" ["argument"] 1)
    '("multi" ["argument"] 1)
    '("dl" ["argument"] 0)
    '("chemin" ["argument"] 0)
    '("tend" ["argument"] 0)
    '("eq" ["argument"] 0)
    '("The" ["argument"] 1)
    '("suite" ["argument"] 1)
    '("codeCorrige" 1)
    '("cor" 1)
    '("dem" 1)
    '("defin" 1)
    '("prop" 1)
    '("indenteBarre" 1)
    '("norme" 1)
    '("ps" 2)
    '("pe" 1)
    '("entso" 1)
    '("ent" 1)
    '("ensp" 2)
    '("ens" 2)
    '("enum" 1)
    '("exs" 1)
    '("vers" 1)
    '("Oni" 1)
    '("oni" 1)
    '("fl" 1)
    '("Det" 1)
    '("Mataug" 1)
    '("Mat" 1)
    '("binaire" 1)
    '("tr" 1)
    '("cas" 1)
    '("systeme" 1)
    '("cfexo" 1)
    '("cours" 1)
    '("pts" 1)
    '("seulementEnonce" 1)
    '("inter" 1)
    '("comm" 1)
    "dd"
    "calc"
    "infini"
    "vide"
    "cqfd"
    "epsilon"
    "phi"
    "le"
    "leq"
    "ge"
    "geq"
    "int"
    "sujetSuivantCorrigeRV"
    "modeEnonce"
    "modeCorrige"
    "espace"
    "modeCorrigeRV"
    "sujetSuivant"
    "modeRapportDeColle"
    "tvi"
    "cad"
    "càd"
    "d"
    "dt"
    "dx"
    "dy"
    "dz"
    "rien"
    "nb"
    "ds"
    "som"
    "sumi"
    "somi"
    "sumj"
    "sumk"
    "maj"
    "rg"
    "id"
    "de"
    "init"
    "here"
    "kx"
    "diag"
    "can"
    "implique"
    "equi"
    "sinon"
    "NB"
    "inv"
    "rema"
    "remas"
    "ex"
    "preambule"
    "N"
    "K"
    "R"
    "Z"
    "Q"
    "C"
    "card"
    "L"
    "eps"
    "lip"
    "F"
    "rpe"
    "M"
    "Gl"
    "ini"
    "héré"
    "tq"
    "et"
    "ou"
    "non"
    "dc"
    "grad"
    "regexp"
    "setminus"
    "prochainePageImpaire"
    "eqni"
    "limn"
    "absurde"
    "modePython"
    "modeCaml"
    "codePrefixe"
    "codeSuffixe"
    "langage"
    "annalesOption"
    "CCP")
   (LaTeX-add-environments
    '("colle" LaTeX-env-args ["argument"] 0)
    "Exercice"
    "exemple"
    "petitComm"
    "vie"
    "interlude"
    "probleme"
    "solutionCours"
    "pNiceMatrixAug"
    "demo"
    "preuve"
    "theoreme"
    "definition"
    "definitions"
    "exe"
    "proposition"
    "lemme"
    "corollaire")
   (LaTeX-add-saveboxes
    "frise")
   (LaTeX-add-xcolor-definecolors
    "commentsColor"
    "keywordsColor"
    "stringColor")
   (LaTeX-add-caption-DeclareCaptions
    '("\\DeclareCaptionFont{white}" "Font" "white")
    '("\\DeclareCaptionFont{red}" "Font" "red"))
   (LaTeX-add-fontspec-newfontcmds
    "ttm"))
 :latex)

