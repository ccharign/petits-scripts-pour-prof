(global-set-key [?\C-ê]  (make-keymap))


;; Raccourcis clavier pensés pour clavier bépo
(global-set-key [?\C-ê ?\C-g]  'keyboard-quit)
(global-set-key (kbd "C-ê a")  '(lambda () "" (interactive) (insert-char #x3b1 1 )))  ; α \alpha
(global-set-key (kbd "C-ê b")  '(lambda () "" (interactive) (insert-char #x3b2 1 )))  ; β \beta

(global-set-key (kbd "C-ê g")  '(lambda () "" (interactive) (insert-char #x3b3 1 )))  ; γ \gamma
(global-set-key (kbd "C-ê d")  '(lambda () "" (interactive) (insert-char #x3b4 1 )))  ; δ \delta
(global-set-key (kbd "C-ê e")  '(lambda () "" (interactive) (insert-char #x3b5 1 )))  ; ε \varepsilon
(global-set-key (kbd "C-ê l")  '(lambda () "" (interactive) (insert-char #x3bb 1 )))  ; λ \lambda
(global-set-key (kbd "C-ê L")  '(lambda () "" (interactive) (insert "Λ" )))  ; λ \lambda
(global-set-key (kbd "C-ê q")  '(lambda () "" (interactive) (insert-char #x3b8 1 )))  ; θ \theta
(global-set-key (kbd "C-ê f")  '(lambda () "" (interactive) (insert-char #x3c6 1 )))  ; φ \phi  φ
(global-set-key (kbd "C-ê j")  '(lambda () "" (interactive) (insert-char #x3c8 1 )))  ; ψ \psi
(global-set-key (kbd "C-ê w")  '(lambda () "" (interactive) (insert-char #x3c9 1 )))  ; ω \omega
(global-set-key (kbd "C-ê r")  '(lambda () "" (interactive) (insert-char #x3c1 1 )))  ; ρ \rho
(global-set-key (kbd "C-ê s")  '(lambda () "" (interactive) (insert-char #x3c3 1 )))  ; σ \sigma
(global-set-key (kbd "C-ê p")  '(lambda () "" (interactive) (insert-char #x3c0 1 )))  ; π \pi
(global-set-key (kbd "C-ê P")  '(lambda () "" (interactive) (insert "Π"  ))) ; Π \Pi
(global-set-key (kbd "C-ê n")  '(lambda () "" (interactive) (insert-char #x3bd 1 )))  ; ν \nu
(global-set-key (kbd "C-ê m")  '(lambda () "" (interactive) (insert-char #x03BC 1 )))  ; µ \mu
(global-set-key (kbd "C-ê t")  '(lambda () "" (interactive) (insert-char #x3c4 1 )))  ; τ \tau
(global-set-key (kbd "C-ê z")  '(lambda () "" (interactive) (insert-char #x3b6 1 )))  ; ζ \dzeta
(global-set-key (kbd "C-ê X")  '(lambda () "" (interactive) (insert-char #x3be 1 )))  ; ξ \xi
(global-set-key (kbd "C-ê h")  '(lambda () "" (interactive) (insert-char #x3b7 1 )))  ; η \eta
(global-set-key (kbd "C-ê D")  '(lambda () "" (interactive) (insert-char #x394 1 )))  ; Δ \Delta
(global-set-key (kbd "C-ê F")  '(lambda () "" (interactive) (insert-char #x3a6 1 )))  ; Φ \Phi
(global-set-key (kbd "C-ê J")  '(lambda () "" (interactive) (insert-char #x3a8 1 )))  ; Ψ \Psi
(global-set-key (kbd "C-ê O")  '(lambda () "" (interactive) (insert-char #x3a9 1 )))  ; Ω \Omega
(global-set-key (kbd "C-ê G")  '(lambda () "" (interactive) (insert-char #x393 1 )))  ; Γ \Gamma
(global-set-key (kbd "C-ê Q")  '(lambda () "" (interactive) (insert-char #x398 1 )))  ; Θ \Theta
(global-set-key (kbd "C-ê S")  '(lambda () "" (interactive) (insert-char #x3a3 1 )))  ; Σ \Sigma
(global-set-key (kbd "C-ê o")  '(lambda () "" (interactive) (insert-char #x2218 1 )))  ; ∘ \circ

(global-set-key (kbd "C-à p")  '(lambda () "" (interactive) (insert-char #x2264 1 )))  ; ≤ \le
(global-set-key (kbd "C-à g")  '(lambda () "" (interactive) (insert-char #x2265 1 )))  ; ≥ \ge
(global-set-key (kbd "µ")  '(lambda () "" (interactive) (insert-char #x03BC 1 )))  ; µ dans les lettres grecques

; les lignes suivantes sont inspirées des raccourcis proposés par le mode "maths" de AucTeX
(global-set-key (kbd "C-ê I")  '(lambda () "" (interactive) (insert-char #x221e 1 )))  ; ∞ \infty

(global-set-key (kbd "C-ê i")  '(lambda () "" (interactive) (insert-char #x2208 1 )))  ; ∈ \in
(global-set-key (kbd "C-ê N")  '(lambda () "" (interactive) (insert-char #x2115 1 )))  ; ℕ \N
(global-set-key (kbd "C-ê R")  '(lambda () "" (interactive) (insert-char #x211d 1 )))  ; ℝ \R
(global-set-key (kbd "C-ê Z")  '(lambda () "" (interactive) (insert-char #x2124 1 )))  ; ℤ \Z
(global-set-key (kbd "C-ê C")  '(lambda () "" (interactive) (insert-char #x2102 1 )))  ; ℂ \C
(global-set-key (kbd "C-ê .")  '(lambda () "" (interactive) (insert-char #x22c5 1 )))  ; ⋅ \cdot


(global-set-key (kbd "C-à v")  '(lambda () "" (interactive) (insert-char #x2205 1 )))  ; ∅ \emptyset
(global-set-key (kbd "C-à <right>")  '(lambda () "" (interactive) (insert-char #x21d2 1 )))  ; ⇒ \Rightarrow
(global-set-key (kbd "C-à <left>")  '(lambda () "" (interactive) (insert-char #x21d0 1 )))  ; ⇐ \Leftarrow
(global-set-key (kbd "C-à =")  '(lambda () "" (interactive) (insert-char #x21d4 1 )))  ; ⇔ \LeftRightarrow

(global-set-key (kbd "C-à <down>")  '(lambda () "" (interactive) (insert-char #x2229 1 )))  ; ∩ \cap
(global-set-key (kbd "C-à <up>")  '(lambda () "" (interactive) (insert-char #x222a 1 )))  ; ∪ \cup
(global-set-key (kbd "C-à c")  '(lambda () "" (interactive) (insert-char #x2282 1 )))  ; ⊂ \subset
(global-set-key (kbd "C-à t")  '(lambda () "" (interactive) (insert-char #x2283 1 )))  ; ⊃ \supset
(global-set-key (kbd "C-à \\")  '(lambda () "" (interactive) (insert-char #x2216 1 )))  ; ∖ \setminus
(global-set-key (kbd "C-à (")  '(lambda () "" (interactive) (insert "\\left" )))
(global-set-key (kbd "C-à )")  '(lambda () "" (interactive) (insert "\\right" )))
(global-set-key (kbd "C-à r")  '(lambda () "" (interactive) (insert "\\sqrt{" )))
(global-set-key (kbd "C-à t")  '(lambda () "" (interactive) (insert "\u2200" ))) ; ∀
(global-set-key (kbd "C-à e")  '(lambda () "" (interactive) (insert "\u2203" ))) ; ∃
(global-set-key (kbd "C-à m")  '(lambda () "" (interactive) (insert "\u21A6" ))) ; ↦
(global-set-key (kbd "C-à g")  '(lambda () "" (interactive) (insert "←" ))) ;←

(global-set-key (kbd "C-à é")  '(lambda () "" (interactive) (insert "∧" ))) ; et
(global-set-key (kbd "C-à o")  '(lambda () "" (interactive) (insert "∨" ))) ; ou
(global-set-key (kbd "C-à n")  '(lambda () "" (interactive) (insert "¬" ))) ; non


;; Musique
;; https://unicode-table.com/fr/blocks/musical-symbols/
(global-set-key (kbd "C-é b")  '(lambda () "" (interactive) (insert "𝅗𝅥" ))) ; blanche
(global-set-key (kbd "C-é n")  '(lambda () "" (interactive) (insert "𝅘𝅥" ))) ; noire
(global-set-key (kbd "C-é c")  '(lambda () "" (interactive) (insert "𝅘𝅥𝅮" ))) ; croche
(global-set-key (kbd "C-é s")  '(lambda () "" (interactive) (insert "𝄽" ))) ; soupir
(global-set-key (kbd "C-é d")  '(lambda () "" (interactive) (insert "𝄾" ))) ; demi soupir


(defun insert-frac ()
  "Insert a command \\frac of LaTeX."
  (interactive)
  (insert "\\frac{}{denom}")
  (backward-char 8)
  )

(defun go-to-denom ()
  "Go to the next string 'denom' and delete it."
  (interactive)
  (search-forward "denom")
  (delete-backward-char 5)
  )


(add-hook 'LaTeX-mode-hook
	  (function
	   (lambda ()
             (local-set-key (kbd "C-à f") 'insert-frac)
             (local-set-key (kbd "C-à d") 'go-to-denom))
	   )
	  )

