Compilation latex pour un devoir ou des colles.

 - Pour un devoir (lancé par défaut si le nom du fichier contient "DS" ou "pb", sinon --devoir) : création de deux pdf, l'un avec l'énoncé et indications, l'autre avec le corrigé.
 Si le nom du fichier contient "pb", ou si l'option --problème est précisée, le fichier .tex n'est pas supposé contenir d'entête, ni de \begin{document} ni \end{document}
 
 - Pour une colle (lancé par défaut si le nom du fichier contient "colle") : création d'un pdf contenant (n+1) énoncés et n corrigés. La classe, la semaine et la date sont automatiquement récupérés. Options :
   `-n` : nombre de groupes
   `-N` --nettoyer : pour N=1, les fichiers auxiliaires sont supprimés. Pour N=2 tous les pdf le sont également.
   `-c` --copie : copie sur clef USB. Les répertoires correspondant doivent être renseignés en variable globale dans RÉPS_CLEF
   `-g` --git : add, commit, push
   `--rv` : mise en page adaptée à une impressin recto-verso
   `--normale` : compilation simple.
   `--sansTDM` : sans table des matières pour le corrigé.

 - Le script nettoyageTex.py permet de supprimer tous les fichiers auxiliaires dans un répertoire.

Dans tous les cas, les indications doivent être dans des environement nommés « indication » et les corrigés dans des environement « solution ». Chaque indication et solution peut être tapée immédiatement après la question correspondante.