#! /usr/bin/python3
# -*- coding: utf-8 -*-

import glob #donne toutes les adresses des fichiers d'un repertoire
import os
import sys
import shutil #Manipulation de fichiers plus complexes que dans os.
import argparse

def prefixe(p,m):
    if len(p)>len(m):
        return False
    else:
        for i, x in enumerate(p):
            if x!=m[i]:
                return False
        return True

def suffixe(s,m):
    if len(s)>len(m):
        return False
    else:
        for i in range(len(s)):
            if s[-1-i]!=m[-1-i]:
                return False
        return True

def nettoyage(chemin,aussiPdf=False):
    """Élimine les fichier .aux, .log, .out, les répertoires "build" et aussi les pdf si l'option est activée. """
    l=glob.glob(chemin+'/*')
    n=0
    for f in l:
        if os.path.isdir(f):
            if os.path.basename(f)=="build":
                shutil.rmtree(f)
                n+=1
            else:
                n+=nettoyage(f, aussiPdf=aussiPdf)
        else:
            if os.path.splitext(f)[1] in [ '.aux', '.log', '.out', '.toc'] or suffixe('.synctex.gz',f) or (aussiPdf and os.path.splitext(f)[1]==".pdf") or prefixe( "sol", os.path.basename(f)) or prefixe( "indic", os.path.basename(f))  :
                os.remove(f)
                n+=1
    return n   #bonus: renvoie le nombre de fichiers supprimés





if len(sys.argv)>1:
        chemin= sys.argv[1]
else:
        chemin=os.getcwd()
        print("Nettoyage du répertoire courant, càd ",chemin)

n=nettoyage(chemin,aussiPdf=True)
print( n , " fichiers supprimés")
