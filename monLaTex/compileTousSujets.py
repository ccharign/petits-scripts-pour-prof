#! /usr/bin/python3
# -*- coding: utf-8 -*-

import os
import glob

import subprocess


ici=os.getcwd()

for f in glob.glob(ici+'/*'):
    if "sujet" in f and os.path.splitext(f)[1]==".tex":
        print(f)
        subprocess.call(["monLaTex.py", f])
        
