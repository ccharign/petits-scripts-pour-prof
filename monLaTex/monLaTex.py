#!/usr/bin/python3
# -*- coding: utf-8 -*-


import os
import subprocess
import argparse  # Pour gérer les arguments plus finement qu'avec sys.argv
import re  # Expressions régulières
import datetime
import glob
import shutil  # pour supprimer un répertoire (shutil.rmtree)


# A FAIRE :


## --N=2  : demander confirmation avant d'effacer les pdf

## compile normale sans entête


## copie autres doc : faire un dico

## Chercher le nom des élèves dans le colloscope
#      -> fichier contenant le colloscope
#      -> créer le fichier colle à remplir
#      -> Interface pour choisir les élèves qui auront le même sujet ?

## -c pour des fichiers quelconques


OÙ_COPIER = {"TC1": ["/media/moi/Cyril/TC1", "/media/removable/CYRIL/TC1/"],
             "TC2": ["/media/moi/Cyril/TC2", "/media/removable/CYRIL/TC2/"],
             "Option MP": ["/media/moi/Cyril/Option MP", "/media/removable/CYRIL/Option MP/"]
             }  # Pas encore utilisé...

RÉPS_CLEF = ["/media/moi/disk/colles"]  # Si plusieurs on fera tous les essais...







#### PARAMÉTRAGE ####

N_DEFAUT = 2  # nb de copies par défaut pour les colles
DICO_JOUR = {"MP": 2, "MPSI": 4, "MPI": 2}  # Jour de la semaine de chaque colle. Lundi=0.
NB_GROUPES = {"MP": 1, "MPSI": 2, "MPI": 2}
RÉP_GIT = "/home/moi/enseignement"


class CD():
    def __init__(self, chemin_dossier):
        self.rép = chemin_dossier
    def __enter__(self):
        self.rép_initial = os.getcwd()
        os.chdir(self.rép)
    def __exit__(self, type, value, traceback):
        os.chdir(self.rép_initial)


def rép_build_nom(chemin):
    """ Crée un répertoire build, et renvoie (chemin absolu du répertoire, de build, et le nom du fichier sans extension)"""
    répertoire = os.path.dirname(os.path.abspath(chemin))
    build = répertoire+"/build/"
    os.makedirs(build, exist_ok=True)
    nom = os.path.splitext(os.path.basename(chemin))[0]
    return répertoire, build, nom



   
def pour_devoir(chemin, sansTOC, debug=False):
    """ Crée un pdf d'énoncé et un pdf de corrigé.
    En pratique, l'un aura la commande \modeEnonce et l'autre \modeCorrige, qui doivent exister dans le préambule latex."""
   
    répertoire, build, nom = rép_build_nom(chemin)

    entree = open(chemin, "r")
    chemin_sol = build+nom+"Corrige.tex"
    sortie_sol = open(chemin_sol, 'w')
    chemin_enonce = build+nom + "Enonce.tex"
    sortie_enonce = open(chemin_enonce, 'w')

    if debug:
        print("nom du fichier : ", nom, "\n")
        print("pour corrigé : ", chemin_sol, "\n")
        print("pour énoncé : ", chemin_enonce, "\n")
        
    def écrit_ligne(l):
        """ Recopie la ligne l dans les deux fichiers : corrigé et énoncé."""
        sortie_sol.write(l)
        sortie_enonce.write(l)

    ligne=entree.readline().strip()
    while ligne!=r"\begin{document}":
        if ligne!=r"\modeCorrige":écrit_ligne(ligne+"\n")
        ligne=entree.readline().strip()
        
    écrit_ligne(ligne+"\n") # Le \begin{document}
    sortie_sol.write(r"\modeCorrige"+"\n")
    sortie_enonce.write(r"\modeEnonce"+"\n")
    if not sansTOC :  sortie_sol.write(r"\tableofcontents"+"\n")

    for ligne in entree:
        if ligne.strip()!=r"\modeCorrige":écrit_ligne(ligne)

    sortie_sol.close()
    sortie_enonce.close()
    entree.close()
    compileFinale([chemin_enonce, chemin_sol], build, répertoire)


def pour_problème(chemin, sansTOC, debug=False):
    """ On prend ici un fichier .tex qui n'a pas d'entête ni de \begin\end{document}.
    Crée un pdf d'énoncé et un pdf de corrigé."""
    répertoire, build, nom = rép_build_nom(chemin)

    entree=open(chemin, "r")
    chemin_sol=build+nom+"Corrige.tex"
    sortie_sol=open(chemin_sol , 'w')
    chemin_enonce=build+nom + "Enonce.tex"
    sortie_enonce=open( chemin_enonce, 'w')
        
    def écrit_ligne(l):
        """ Recopie la ligne l dans les deux fichiers : corrigé et énoncé."""
        sortie_sol.write(l)
        sortie_enonce.write(l)

    écrit_ligne("\\documentclass{mon-style}\n\n\\begin{document}\n\n")
    sortie_sol.write(r"\modeCorrige"+"\n")
    sortie_enonce.write(r"\modeEnonce"+"\n")
    if not sansTOC: sortie_sol.write(r"\tableofcontents"+"\n")

    for ligne in entree:
        if ligne.strip()!=r"\modeCorrige": écrit_ligne(ligne)

    écrit_ligne(r"\end{document}")
    sortie_sol.close()
    sortie_enonce.close()
    entree.close()
    compileFinale([chemin_enonce, chemin_sol], build, répertoire)


    

    
def prochainJour(jour):
    """ Entrée : jour ∈ [|0, 7[| un jour de la semaine
        Sortie : la date de ce prochain jour."""
    aujourdhui = datetime.date.today()
    àAttendre = (jour-aujourdhui.weekday())%7
    return aujourdhui + datetime.timedelta(àAttendre)
    

def pour_colle(chemin, nb=4, recto_verso=True, copierSurClef=False):
    """ Crée un pdf contenant (nb+1) énoncés et nb corrigés. Si nb==-1, on prend la valeur dans NB_GROUPES.
        Le fichier d'entrée ne doit contenir que les exercices : pas l'entête ni le \begin et \end{document}
        recto_verso : saute des pages en vue d'impression recto-verso PAS FONCTIONNEL POUR L INSTANT
       
    """
        
    répertoire, build, nom = rép_build_nom(chemin)
    
    #Récupérons la classe
    if "MPSI" in nom:
        classe="MPSI"
    elif "MP" in nom:
        classe="MP"
    else:
        print("classe non detectée")
    #nombre de groupes:
    if nb==-1:
        nb=NB_GROUPES[classe]
    #Récupérons la semaine
    semaine=re.search("[0123456789]{1,2}", nom).group() #On recherche un ou deux chiffres
    #Récupérons la date
    date=prochainJour(DICO_JOUR[classe])
    dateStr = date.strftime("%d-%m-%Y")
    
    cheminSortie = build+nom+".tex"
    
    entrée=open(chemin,'r')
    sortie=open(cheminSortie, 'w')
    
    contenu=entrée.readlines()
    def écrire_une_copie():
        sortie.write(r"\thispagestyle{empty}"+"\n")
        for l in contenu :sortie.write(l)
        
    sortie.write(r"""
\documentclass{mon-style}
\begin{document}
\Opensolutionfile{sol}[sol]
\Opensolutionfile{indic}[indic]
     """ )
    sortie.write(r"\def\classe{"+classe+" }")
    sortie.write(r"\def\semaine{"+semaine+"}")
    sortie.write(r"\def\date{"+dateStr+"}")

    ## Les énoncés ##
    écrire_une_copie()
    for i in range(nb):
        sortie.write(r"\setcounter{Exercice}{0}")
        if recto_verso:
            sortie.write(r"\prochainePageImpaire")
        else:
            sortie.write(r"\newpage")
        écrire_une_copie()
        sortie.write("\n")

        
    ### Les corrigés ###
    
    sortie.write(r"""
\Closesolutionfile{sol}
\Closesolutionfile{indic}
""")
    if recto_verso: sortie.write(r"\modeCorrigeRV")
    else: sortie.write(r"\modeCorrige")


    for i in range(nb):
        if recto_verso : sortie.write(r"\newpage\prochainePageImpaire")
        sortie.write(r"""
\setcounter{Exercice}{0}

\hrule\smallskip
\hrule\medskip

""")
        écrire_une_copie()
        

    sortie.write(r"\end{document}")
    sortie.close()

    compileFinale([cheminSortie], build, répertoire)
    
    if copierSurClef:
        pdf = nom + ".pdf"
        copie(pdf,répertoire)
        
        
def copie(nomFichier, répertoireDépart, ListeRépertoireArrivée=RÉPS_CLEF):
    """ Effectue une copie vers chacun des répertoires de  ListeRépertoireArrivée.
    Affiche un avertissement si aucun n'a fonctionné."""
    cheminDépart=os.path.join(répertoireDépart, nomFichier)
    réussis=0
    for rép in ListeRépertoireArrivée:
        essai = subprocess.run(["cp", cheminDépart, os.path.join(rép, nomFichier)], capture_output="True")
        if essai.returncode==0:
            réussis +=1
            print( f"Copie vers {rép}")
        semaine = int(re.search("[0123456789]{1,2}", nomFichier).group())
        for semaine_préc in range(1, semaine):
            pdfPréc = nomFichier.replace(semaine_préc, str(semaine-1))
            if os.path.exists(pdfPréc):
                print(f"J'efface {pdfPréc}")
                subprocess.run(["rm", os.path.join(rép, pdfPréc)])
    if réussis==0:
        print("Échec de la copie vers", ListeRépertoireArrivée)


def compileFinale(fichiers, build, rép, n=1):
    """ Compile les fichiers .tex dans la liste passée en argument.
        build est le dossier où sont envoyés les fichiers créés.
        rép est le répertoire où les pdf seront déplacés.
        n : nb de compilations.
    Compilation effectuée par lualatex"""
    
    os.chdir(build)
    for f in fichiers:
        for _ in range(n): subprocess.call(["lualatex", "-shell-escape", f])
        pdf=os.path.splitext(os.path.split(f)[1])[0]+".pdf" #nom du fichier pdf créé
        subprocess.call(["mv",os.path.join(build, pdf),os.path.join(rép, pdf)]) # Les pdf sont déplacés de build vers le répertoire principal
        subprocess.call(["wmctrl","-a", pdf]) # Focus sur l'éditeur pdf (si déjà ouvert)
    os.chdir(rép)


def compile_normale(chemin, n=1):
    """ Compilation classique. Crée un répertoire build pour les fichiers annexes.
        n : nb de compilations """
    rép, build, nom = rép_build_nom(chemin)
    tex=nom+".tex"
    subprocess.call(["cp",os.path.join(rép, tex),os.path.join(build, tex)])
    os.chdir(build)
    for _ in range(n) : subprocess.call(["lualatex", "-shell-escape", os.path.join(build, tex)])
    os.chdir(rép)
    pdf=nom+".pdf"
    os.rename( os.path.join(build, pdf), os.path.join(rép, pdf)) # Les pdf sont déplacés de build vers le répertoire principal
    subprocess.call(["wmctrl","-a", pdf]) # Focus sur l'éditeur pdf (si déjà ouvert)




### Partie parseur ###

def récupère_arguments():
    parseur=argparse.ArgumentParser()
    
    parseur.add_argument("chemin", help="fichier à compiler", type=str)
    parseur.add_argument("-n","--nombre", help="nombre d'énoncés dans le cas d'une colle. Par défaut -1, qui signifie « prendre la valeur par défaut de la classe ».", type=int, default=-1)
    parseur.add_argument("-N", "--nettoyer", help="1 : supprime les fichiers intermédiaires, 2 : y compris *tous* les pdf du dossier", type=int, default=0)
    parseur.add_argument("-c",  "--copie", help="copier sur la clef", action="store_true", default=False)
    parseur.add_argument("--rv", help="Pour une impression recto-verso", type =int, default=1)
    parseur.add_argument("--normale", help="compile simple", action="store_true", default=False)
    parseur.add_argument("--problème", help="un énoncé et un corrigé. Le fichier .tex n'a pas les entête (\documentclass et \begin{document}.", action="store_true", default=False)
    parseur.add_argument("--devoir", help="un énoncé et un corrigé, le fichier .tex a les entêtes.", action="store_true", default=False)
    parseur.add_argument("--sansTDM", help="pas de table des matières dans le corrigé", action="store_true", default=False) 
    parseur.add_argument("--nbCompil", help="nombre de compilations", type=int, default=1)
    parseur.add_argument("-g", "--git", help="add, commit et push le document", action="store_true", default=False)
    return parseur.parse_args()


if __name__ == "__main__":
    args = récupère_arguments()
    print(args.__dict__)
    
    chemin=args.chemin
    if chemin[-1]==".":
        chemin+="tex"
    elif chemin[-4:]!=".tex":
        chemin+=".tex"
    nomFichier=os.path.splitext(os.path.basename(chemin))[0]
    print(nomFichier)
    
    if args.normale:
        compile_normale(chemin, n = args.nbCompil)
    elif "pb" in nomFichier or args.problème or "DS" in nomFichier:
        pour_problème(chemin, args.sansTDM)
    elif args.devoir  or "DM" in nomFichier  or "sujet" in nomFichier:
        pour_devoir(chemin, args.sansTDM)
    elif "colle" in nomFichier:
        pour_colle(chemin, nb=args.nombre, copierSurClef=args.copie, recto_verso=bool(args.rv) )
    else: 
        compile_normale(chemin, n = args.nbCompil)
        
    if args.git:
        with CD(RÉP_GIT):
            print("\n\n Mise à jour du dépôt git")
            subprocess.call( ["git", "pull"])
            subprocess.call( ["git", "add", "*.tex","*.ml", "*.py" ])
            subprocess.call( ["git", "commit", "-m", f"Lors de la compilation de {nomFichier}"])
            subprocess.call( ["git", "push"])

    if args.nettoyer>0:
        shutil.rmtree( "build")        
    if args.nettoyer==2:
        àSupprimer = glob.glob("*.pdf")
        print("Fichiers à supprimer ", àSupprimer)
        for f in àSupprimer:
            os.remove(f)
