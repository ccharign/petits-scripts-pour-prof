(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(tango-dark))
 '(font-use-system-font t)
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(rjsx-mode tide typescript-mode web-mode auctex python-black yasnippet projectile hydra flycheck company avy which-key helm-xref zenburn-theme json-mode))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 113 :width normal)))))

(set-language-environment "UTF-8")

;Parenthèse fermante auto
(electric-pair-mode 1)

;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
;(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line



;;; Emacs Load Path
(setq load-path (cons "~/.emacs.d/lisp" load-path))

;; Une nouvelle lettre remplace la sélection
(delete-selection-mode 1)

(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Pas d’indentation avec des tabs
(setq-default indent-tabs-mode nil)

;; Pas de fichiers~
( setq make-backup-files nil)


;; ;; montrer des filets sur les indentations
;; (add-to-list 'load-path "~/.emacs.d/packages/highlight-indents/")
;; (require 'highlight-indentation)

;;Déplacer des bouts de code
;(drag-stuff-global-mode 1);
;(drag-stuff-define-keys);


;; Paquets
(require 'package)
(package-initialize)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)




;; (setq package-selected-packages '(lsp-mode yasnippet lsp-treemacs helm-lsp projectile hydra flycheck company avy which-key helm-xref dap-mode zenburn-theme json-mode))
;; (when (cl-find-if-not #'package-installed-p package-selected-packages)
;;   (package-refresh-contents)
;;   (mapc #'package-install package-selected-packages))
;(load-theme 'zenburn t)

(helm-mode)
(require 'helm-xref)
(define-key global-map [remap find-file] #'helm-find-files)
(define-key global-map [remap execute-extended-command] #'helm-M-x)
(define-key global-map [remap switch-to-buffer] #'helm-mini)
;(setq helm-display-function 'helm-display-buffer-in-own-frame)

(which-key-mode)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
       ; company-idle-delay 0.0
       ; company-minimum-prefix-length 1
      create-lockfiles nil) ;; lock files will kill `npm start'



;; web-mode
; https://gist.github.com/CodyReichert/9dbc8bd2a104780b64891d8736682cea
;; (add-to-list 'auto-mode-alist '("\\.jsx?$" . web-mode)) ;; auto-enable for .js/.jsx files
;; (setq web-mode-content-types-alist '(("jsx" . "\\.ts[x]?\\'")))
;; (add-to-list 'auto-mode-alist '("\\.tsx?$" . web-mode)) ;; auto-enable for .js/.jsx files
;; (setq web-mode-content-types-alist '(("tsx" . "\\.ts[x]?\\'")))


; disable default checker jslint
;; (setq-default flycheck-disabled-checkers
;;               (append flycheck-disabled-checkers
;;                       '(javascript-jshint json-jsonlist)))
;; Enable eslint checker for web-mode
;; (flycheck-add-mode 'javascript-eslint 'web-mode)

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1))

;; if you use treesitter based typescript-ts-mode (emacs 29+)
(add-hook 'typescript-ts-mode-hook #'setup-tide-mode)


;; Enable flycheck globally
(add-hook 'after-init-hook #'global-flycheck-mode)



;; Python
;(setq elpy-rpc-python-command "/usr/bin/python3")
;(elpy-enable)
;; (when (require 'flycheck nil t)
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
;;   (add-hook 'elpy-mode-hook 'flycheck-mode))

(use-package eglot
  :ensure t
  :defer t
  :hook (python-mode . eglot-ensure)
  :bind (:map eglot-mode-map
              ("C-c C-d" . eldoc)
              ("C-c C-l C-r" . eglot-rename)
              ("C-c C-l C-i" . python-sort-imports)
              ("C-c C-l C-f" . eglot-format-buffer)
              ("C-C C-l C-q" . eglot-code-action-quick-fix)
              ("C-C C-l C-a" . eglot-code-actions)
              )
  )

(add-hook 'Python-mode-hook
          (lambda ()
            (local-set-key [C-c TAB] 'python-black-buffer)))




;; Latex

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq TeX-auto-save t) ; lecture du fichier à la sauvegarde
(setq TeX-parse-self t) ; à l'ouverture

(add-hook 'LaTex-mode-hook 'ispell)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(setq ispell-dictionnary "french")

(eval-after-load 'latex '(add-to-list 'LaTeX-verbatim-environments "lstlisting"))
(eval-after-load 'latex '(add-to-list 'LaTeX-verbatim-environments "minted"))
(eval-after-load 'latex '(add-to-list 'LaTeX-verbatim-environments "Piton"))

;; Chargement des raccourcis clavier
(load "clavier")


;(add-hook 'Python-mode-hook
;	  (function
;	   (lambda ()
;             (global-set-key (kbd "C-à f") (insert "### fin ###" ))
;	     ))
;	  )


; mode mineur abbrev
(define-abbrev-table 'global-abbrev-table '(
                                            ("\\pm" "±")
                                            ("\\sum" "∑")
                                            ("\\prod" "∏" )
                                            ("\\int" "∫")
                                            ("\\partial" "∂")
                                            )
  "custom abbrev table pour LaTeX"
  :regexp "\\(\\(\\\\[^[:space:]]+\\)\\|\\(\\^[0-9]\\)\\)")

(add-hook 'TeX-mode-hook (function (lambda () (abbrev-mode))))



