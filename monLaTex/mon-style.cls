
\NeedsTeXFormat{LaTeX2e}%
\ProvidesClass{mon-style}%[20/04/2019, v1.0]%
\AtEndOfClass{\RequirePackage{microtype}}%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}%
\ProcessOptions*%
\LoadClass[10pt]{article}%
\let\epsilon\varepsilon

%========================================
% packages
%========================================
%\RequirePackage{ifluatex, ifxetex}
%\ifluatex
\RequirePackage{shellesc}
%\else\ifxetex
%\relax  
%\else % vieux TeX
%  \RequirePackage[utf8]{inputenc} 		% utf8
%  \RequirePackage[T1]{fontenc} 			% encodage 8-bit avec 256 glyphes
%\fi\fi
\RequirePackage{lmodern}
\RequirePackage[french]{babel} 		% français
\RequirePackage{xspace}
%\RequirePackage{amsmath}
\RequirePackage{changepage} %Savoir si on est page paire ou impaire. Nécessite deux compilations
\RequirePackage{geometry} 				% géométrie de la page

\RequirePackage{graphicx} 				% graphiques et couleurs
\graphicspath{{../}} %Chercher les graphiques dans le répertoire parent
\RequirePackage{fancyhdr}  				% entêtes et bas de pages
\RequirePackage{multicol,lscape}  	
\RequirePackage[pdfstartview = FitH, colorlinks, linkcolor=darkgray]{hyperref}% lien hypertextes
\RequirePackage{siunitx} 				% système d'unités SI
%\RequirePackage{MnSymbol}
\RequirePackage{xcolor}					% gestion de couleurs
\RequirePackage{footnote}% à charger après xcolor. Pour notes en bas de page sortant des minipages.
\RequirePackage{subcaption}
\RequirePackage{float}					% pour les flottants
\RequirePackage{enumitem}
\RequirePackage[space]{grffile} % espaces dans les chemins de fichiers
\RequirePackage{witharrows}
\RequirePackage{tikz}					% éléments graphiques tikz
%\RequirePackage{epic,autograph }

\RequirePackage[most]{tcolorbox}
\usetikzlibrary{arrows,automata}		% utile pour les automates, etc
\tikzset{
 initial text=$ $,
}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
%\RequirePackage[scale=1]{ccicons}		% creative commons
%\RequirePackage{titlesec}				% redéfinir les sections, etc 
\RequirePackage{listings}				% affichage des codes
\RequirePackage{minted}
\RequirePackage{piton}
\RequirePackage{amsmath,amsthm,amssymb,amsfonts,esint,%dsfont,
eqnarray }
\RequirePackage{stmaryrd} %Pour le \llbracket
\RequirePackage{diagbox} % Pour la case en haut à gauche d'un tableau
\RequirePackage{csquotes} 				% gestion des chevrons, etc

\RequirePackage[french,linesnumbered]{algorithm2e}
%mettre algorithm2e en français:
\SetKwIF{If}{ElseIf}{Else}{si}{:}{sinon si}{sinon:}{fin du si}
\SetKwFor{While}{tant que}{:}{fin du tant que}%
\SetKwFor{For}{pour}{:}{fin du pour}%
\SetKwProg{Fn}{Fonction}{:}{}
\SetKwInput{Variables}{Variables locales}
\SetKwInput{Precondition}{Précondition}
\SetSideCommentRight
\DontPrintSemicolon
\SetKwInOut{Input}{Entrée}\SetKwInOut{Output}{Sortie}



\RequirePackage{answers}   % indications et solutions
 \Newassociation{indication}{Indication}{indic}
 \Newassociation{solution}{Solution}{sol}
 \theoremstyle{definition}
 \newtheorem{Exercice}{Exercice}
 \newtheorem{exemple}{Exemple}
  
\RequirePackage{lastpage}
\RequirePackage{ifthen}

\PassOptionsToPackage{hyphens}{url}%pour autoriser la césure dans une url... cf stackexchange...
\RequirePackage{hyperref}
%\hypersetup{
%    colorlinks,
%    linkcolor={red!50!black},
%    citecolor={blue!50!black},
%    urlcolor={blue!80!black},
%    pdfencoding=auto
%}
%\ifluatex
  \RequirePackage{unicode-math}
%\else
%  \input{symboles-unicode.tex}		% taper directement des symboles utf8
%\fi



%========================================
% géométrie de la page
%========================================
\geometry{%
	hoffset=0mm, voffset=0mm,%
	paperwidth=210mm, left=15mm, right=15mm,% width = paperwidth - left - right
	paperheight=297mm, top=10mm, bottom=10mm,% height = paperheight - top - bottom
	marginparsep=0mm, marginparwidth=0mm,% textwidth = width - marginparsep - marginparwidth
	headheight=27.8pt, headsep=5mm, footskip=15mm, includehead, includefoot,% textheight = height - headheight - headsep - footskip si les options includehead et includefoot sont présentes
      }%

      
%========================================
% environnements pour les codes
% ========================================

%% minted
\setminted{breaklines=true, mathescape, linenos=true}

% quelques couleurs et réglages
\definecolor{commentsColor}{rgb}{0, .4, 0}
\definecolor{keywordsColor}{rgb}{0.5, 0, 0}
\definecolor{stringColor}{rgb}{0.5, 0, 0}
\DeclareCaptionFont{white}{\color{black}}
\DeclareCaptionFont{red}{\color{red!50!black}}
\newfontfamily\ttm{Dejavu Sans Mono}
%\newfontfamily\ttb{DejaVu Sans Bold}
%
\renewcommand{\lstlistingname}{Code}
\lstset{
  %backgroundcolor = \color{white!97!black},
  basicstyle = \ttm \small,	
  breakatwhitespace = true,         
  breaklines = true,                 
  captionpos = t,                    
  commentstyle = \color{commentsColor},
  escapeinside = {\%*}{*)},          
  extendedchars = true,              
  frame = tb,	                   	 
  framerule = .5pt,				
  keepspaces = false,                 
  keywordstyle = \color{keywordsColor}\bfseries,
  numbers = left,                    
  numbersep = 5pt,                   
  numberstyle = \tiny\color{gray}, 
  rulecolor = \color{black},         
  showspaces = false,                
  showstringspaces = false,          
  showtabs = false,                  
  stepnumber = 1,                    
  stringstyle = \color{stringColor}, 
  tabsize = 2,	                   
  columns = fixed,                    
  postbreak = \mbox{\textcolor{commentsColor}{$\hookrightarrow$}\space},
  mathescape,
  escapebegin=\color{commentsColor}
  %upquote = true
}
%----------------------------------------
% caractères spéciaux
% \lstset{literate =
%   {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
%   {œ}{{\oe}}1
%   {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
%   {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
%   {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
%   {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
%   {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
%   {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
%   {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
%   {α}{{\alpha}}1 {η}{{\eta}}1 
%   {Ã}{{\~A}}1 {ã}{{\~a}}1 {Õ}{{\~O}}1 {õ}{{\~o}}1
%   {∈}{{$\in$}}1
%   {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
%   {ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
%   {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
%   {€}{{\euro}}1 {£}{{\pounds}}1 {«}{{\guillemotleft}}1
%   {»}{{\guillemotright}}1 {ñ}{{\~n}}1 {Ñ}{{\~N}}1 {¿}{{?`}}1
% }
%----------------------------------------


%\newfontfamily\ttm{OpenSans Regular} % Pour avoir des lettres grecques dans les listings, il faut une fonte avec les lettres grecques...
%\newfontfamily\ttb{OpenSans Bold}






% -------- réglages -------------

\frenchbsetup{ItemLabeli=$\bullet$}
\frenchbsetup{ItemLabelii=$\diamond$}
\frenchbsetup{ItemLabeliii=$\circ$}

\setlength\delimitershortfall{-1 pt}%pour que les délimiteurs dépassent leur contenu
\let\epsilon=\varepsilon
\let\phi=\varphi
\let\le=\leqslant
\let\leq=\leqslant
\let\ge=\geqslant
\let\geq=\geqslant
\def\int{\intop\nolimits}





%% frise, aussi par François
\usepackage{expl3}
\makeatletter
\ExplSyntaxOn

\cs_new:Nn \frise:n % l'argument est la longueur nominale de la frise 
                    % en fait, la frise sera plus longue de 1 pt (environ)
     {\dim_set:Nn \l_tmpa_dim {#1} 
      %
      % nombre de motifs de la frise :
      \int_set:Nn \l_tmpa_int {\dim_ratio:nn {\l_tmpa_dim + 3.1415pt} {6.2831pt} }
      %
      \dim_set:Nn \l_tmpb_dim {6.2831pt * \l_tmpa_int}
      %
      \frise_bis:nn \l_tmpa_int {\fp_to_decimal:n {\l_tmpa_dim / \l_tmpb_dim}}
     }

% le premier argument est le nombre de motifs, le deuxième est le coefficient de dilatation
% ce coefficient de dilation est très proche de 1 pour avoir une frise
% qui aura exactement la longueur nominale demandée
\cs_new:Nn \frise_bis:nn
     {\begin{tikzpicture}[scale = 0.0352778, yscale = #2]
      \foreach \k in {1,...,#1} 
        {\begin{scope}[shift={(0,\k * (-6.2831))}]
           \fill
           (0,0) .. controls (0.5,0.5)     and (1,1)         .. (1,1.5708)
                 .. controls (1,2.1415)    and (0.5,2.6415)  .. (0,3.1415)
                 .. controls (-0.5,3.6415) and (-1,4.1415)   .. (-1,4.7123)
                 .. controls (-1,5.2815)   and (-0.5,5.7830) .. (0,6.2830)
                 -- (1.4,7.2830) % ici, on dépasse le 6.2830 de 1 pt
                 .. controls (0.9,6.7830)  and (0.4,6.2815)  .. (0.4,5.7123)
                 .. controls (0.4,5.1415)  and (0.9,4.6415)  .. (1.4,4.1415)
                 .. controls (1.9,3.6415)  and (2.4,3.1415)  .. (2.4,2.5708)
                 .. controls (2.4,2)       and (1.9,1.5)     .. (1.4,1); 
         \end{scope}}
      \end{tikzpicture}}


\newsavebox{\frise@box}

\newenvironment {frise}
   {\savenotes
    \mode_if_horizontal:T {\par}
    \parindent \c_zero_skip
    %
    \leavevmode % on a perdu beaucoup de temps avant de mettre ce \leavevmode
    %
    \begin{lrbox}{\frise@box}
    \dim_set:Nn \l_tmpa_dim {\linewidth-8pt}
    \begin{minipage}[t]{\l_tmpa_dim}
    \begin{list}{}{\leftmargin  = \c_zero_skip
                   \rightmargin = \c_zero_skip
                   \itemindent  = \c_zero_skip
                   \topskip     = \c_zero_skip}
    \item}
   {\end{list}
    \dim_gset:Nn \g_tmpa_dim \prevdepth
    \end{minipage}
    \end{lrbox}
    %
    \dim_compare:nNnT \g_tmpa_dim < {2.5 pt}
          {\dim_set:Nn \l_tmpa_dim {2.5 pt - \g_tmpa_dim}
           \sbox{\frise@box}{\vtop{\usebox{\frise@box}
                                   \vskip\l_tmpa_dim}}}
    %
    % hauteur totale de la boîte (et donc longueur nominale de la frise) :
    \dim_set:Nn \l_tmpa_dim 
                {\exp_after:wN \box_dp:N \frise@box + \exp_after:wN \box_ht:N \frise@box}
    %
    \dim_compare:nNnT \l_tmpa_dim > {6.3 pt}
          {\raisebox{-\exp_after:wN \box_dp:N \frise@box}
                    [\exp_after:wN \box_ht:N \frise@box] % on tronque la hauteur de 1 pt
                    {\makebox[8pt][l]{\kern1pt \frise:n \l_tmpa_dim}}}
    \expandafter\box\frise@box
    \spewnotes
    }


\newenvironment{petitComm}%
    {\ifhmode\par\fi
     \unless\if@inlabel\addvspace{\smallskipamount}\fi
     \begin{frise}}
    {\end{frise}
     \vspace{\smallskipamount}}

\ExplSyntaxOff 

\makeatother
%% -------------- frises: fin ---------------------

\NewDocumentEnvironment{vie}{}
  { 
    \begin{itemize}[beginpenalty=10000,leftmargin=2mm]
    \item[]
    \begin{tcolorbox}%
        [
          breakable,
          borderline west={3pt}{0pt}{gray!50, line cap=round},
          frame hidden,
          boxrule=0pt,
          colback=white,
          enhanced,
          top = 0pt,
          bottom = 0pt,
        ]
  }
  {
    \end{tcolorbox}
    \end{itemize}
  }


\newcommand{\comm}[1]{\begin{petitComm}	 #1\end{petitComm}}
\newenvironment{interlude}{\medskip \it }{\medskip}
\newcommand{\inter}[1]{\begin{interlude}\quad #1\end{interlude}}


%----------------------------------------------------
% --------- Gestion des différents modes de compilation---------------
% ----------------------------------------------------

\newenvironment{probleme}{}{}
\newcommand{\seulementEnonce}[1]{#1}
%\newcommand{\seulementEnonce}[1]{#1}
\newenvironment{solutionCours}{\ \\ \textit{Solution :}\color{darkgray} \small}  { }

\def\sujetSuivant{
  \vfill\null\columnbreak
  \vspace{\espace cm}
  \setcounter{Exercice}{0}
}

\def\sujetSuivantCorrigeRV{
  \setcounter{Exercice}{0}
  \newpage
  \prochainePageImpaire
  }

\newcommand{\pts}[1]{#1 pt(s)}

\newcommand{\cours}[1]{\medskip {\bf Cours :} #1\bigskip}
\newenvironment{colle}[1][3]%
{\pagestyle{empty}\begin{landscape} \begin{multicols}{#1}  \setlength{\columnseprule}{.5pt}\setlength{\columnseprule}{.5pt}}%
    {\end{multicols}\end{landscape}}

\def\modeEnonce{
  \renewcommand{\pts}[1]{}
  \Opensolutionfile{sol} % Les solutions seront chargées dans un fichier sol.tex via le paquet answers
  \renewenvironment{indication}{
    
    \textit{Indication :}} {}
} % Les indications par contre sont affichées


\def\modeCorrige{
  \renewcommand{\pts}[1]{}
  \renewenvironment{solution}{\ \\ \textit{solution :}\color{darkgray} \small}  { }  % Solutions affichée
  \renewenvironment{indication}{\color{darkgray} \textit{Indication :} \small}  {\ \\} % Indications affichées
  \renewcommand{\inter}[1]{} %Interludes plus affichés
  \renewcommand{\seulementEnonce}[1]{} 
  \renewenvironment{colle}[1][1]{ \renewcommand{\columnbreak}{\hrule}} { } % Les colles ne sont plus en paysage. Et je remplace le \columnbreak par un \hrule.
  \renewcommand{\cours}[1]{ } % On supprime la question de cours des colles
  \renewcommand{\vfill}{ }
  \def\espace{0}
}

\def\modeCorrigeRV{%mode corrigé pour une colle à imprimer recto-verso
  \modeCorrige
  \def\sujetSuivant{\sujetSuivantCorrigeRV}
}

\def\modeRapportDeColle{
  \renewenvironment{colle}{
    \renewcommand{\columnbreak}{}
    \renewcommand{\sujetSuivant}{\hrule\bigskip{}}
  }{}
  \modeEnonce
}


%----------------------------------------------------
% --------- Raccourcis ----------------
%----------------------------------------------------


% Mettre les def en premier
\def\tvi{théorème des valeurs intermédiaires}

\let\setminus\smallsetminus

\DeclareMathOperator{\e}{e}
\def\cad{\text{c'est-à-dire }}
\def\càd{\text{c'est-à-dire }}
\def\d{\text{d}}
\newcommand{\dd}{\mathop{\mathrm d\null}\mskip-\thinmuskip\mathord{\null}}
\def\dt{\dd t}
\def\dx{\dd x}
\def\dy{\dd y}
\def\dz{\dd z}
\def\rien{}
\def\nb{\noindent \textbf{N.B. }}
\def\ds{\displaystyle}
\def\som{\displaystyle \sum}
\def\sumi{\sum_{i=0}^n}
\def\somi{\displaystyle \sum_{i=0}^n}
\def\sumj{\sum_{j=0}^n}
\def\sumk{\sum_{k=1}^n}
\def\maj{\mathcal}
\def\rg{\text{rg}}
\def\id{\text{Id}}
\def\de{\text{deg}}
\def\init{\textit{Initialisation} : }
\def\here{\textit{Hérédité :}}
%\DeclareMathOperator{\de}{deg}
\def\kx{\K[X]}
%\DeclareMathOperator{\ker}{Ker}
\DeclareMathOperator{\im}{Im}

% Changer l’ancien \Re
\DeclareMathOperator{\Reblabla}{Re}
\renewcommand{\Re}{\Reblabla}

\DeclareMathOperator{\vect}{Vect}
\def\diag{\text{Diag}}
%\def\mat{\text{Mat}}
\DeclareMathOperator{\mat}{Mat}
\def\can{\text{can}}

\DeclareMathOperator{\ch}{ch}
\DeclareMathOperator{\sh}{sh}
\DeclareMathOperator{\thblabla}{th}
\renewcommand{\th}{\thblabla}

\DeclareMathOperator{\GL}{GL}

\def\implique{\Rightarrow}
\def\equi{\Leftrightarrow}
\def\sinon{\text{ sinon }}
\def\NB{\textbf{N.B. } }
\def\inv{^{-1}}
\def\rema{\noindent\textit{Remarque : }}
\def\remas{\noindent\textit{Remarques : }}
\def\ex{\noindent\textit{Exemple : }}
\def\ini{\textbf{Initialisation :}}
\def\héré{\textbf{Hérédité :}}

\def\preambule{ {\it Si vous repérez ce qui vous paraît une erreur d'énoncé, indiquez-le sur votre copie et précisez les initiatives que vous avez été amenés à prendre.  Vous pouvez coder toute fonction complémentaire qui vous semble utile. Dans ce cas indiquez précisément le rôle de cette fonction, la signification de ses paramètres et la nature de la valeur renvoyée.}}

\def\N{\mathbb N}
\def\K{\mathbb K}
\def\R{\mathbb R}
\def\Z{\mathbb Z}
\def\Q{\mathbb Q}
\def\C{\mathbb C}
\renewcommand{\P}{\maj P}
\def\card{\text{Card}}
\def\L{\mathcal L}
\def\eps{\varepsilon}
\def\lip{lipschitzienne}
\def\F{\maj F}

\def\rpe{\R^{+*}}
\def\M{\mathcal{M}}
\def\Gl{\mathcal{G}l}

\def\ini{\textit{Initialisation : }}
\def\héré{\textit{Hérédité : }}
\def\tq{\text{ tq }}
\def\et{\text{ et }}
\def\ou{\:\text{ ou }\:}
%\def\si{\:\text{ si }\:}  Conflit avec siunitx
\def\non{\text{ non }}
\def\dc{\text{ donc }}
\def\grad{\vec\nabla}
\def\regexp{expression régulière}
\def\setminus{∖}

% Reste sur la page actuelle si impair, passe à la suivante sinon
\def\prochainePageImpaire{
  \checkoddpage
  \ifoddpage {}
  \else  {\mbox{}\newpage}
  \fi
}

\newcommand{\cfexo}[1]{\textbf{cf exercice: } \ref{#1}}


\newcommand{\systeme}[1]{\begin{cases}#1\end{cases}}
\newcommand{\cas}[1]{\begin{array}{|ll}#1\end{array}}
% \newcommand{\calc}[2][rlc]{\begin{equationarray*}{#1}
%     \displaystyle #2
%   \end{equationarray*}}
% \newcommand{\calc}[1]{
%   \begin{DispWithArrows*}[displaystyle]
%     #1
%   \end{DispWithArrows*}
% }

\ExplSyntaxOn
\NewDocumentCommand { \calc } { m }
  {
    \group_begin:
    \str_set:Nn \l_witharrows_body_str { #1 }
    \begin{DispWithArrows*}
    #1
    \end{DispWithArrows*}
    \group_end: 
  }
\ExplSyntaxOff

% \newcommand{\tr}[1]{{\vphantom{#1}}^{\mathit t}{#1}}%Vieille notation
\newcommand{\tr}[1]{#1^T}
\newcommand{\binaire}[1]{\overline{#1}^{(2)}}


\newcommand{\Mat}[1]{
  \begin{pmatrix}
    #1
  \end{pmatrix} 
}

\makeatletter %pour matrice augmentée
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
 \hskip -\arraycolsep
 \let\@ifnextchar\new@ifnextchar
 \array{#1}}
\makeatother
%Exemple:
%$\begin{pmatrix}[cc|c]
%  1 & 2 & 3\\
%  4 & 5 & 9
%\end{pmatrix}$

% % Version François
% \makeatletter
% \ExplSyntaxOn
% \NewDocumentEnvironment { pNiceMatrixAug } { O { } }
%   {
%     \begin{pNiceMatrix}[ 
%         #1 ,
%         create-medium-nodes,
%         code-after =
%          {
%            \tikz \draw 
%              ( 
%                [ xshift = \arraycolsep , yshift = 0.3ex ] 
%                nm - \NiceMatrixLastEnv - 1 - \int_eval:n {\c@jCol-1} . north~east 
%              )
%              -- 
%              ( 
%                [ xshift = \arraycolsep , yshift = -0.6ex ] 
%                nm - \NiceMatrixLastEnv - \int_use:N \c@iRow - \int_eval:n {\c@jCol-1} . south~east 
%              ) ;
%          }
%       ]
%   }
%   { \end{pNiceMatrix} }
% \ExplSyntaxOff
% \makeatother

%nouvelle version 03/21
\makeatletter
\ExplSyntaxOn
\NewDocumentEnvironment { pNiceMatrixAug } { }
  { \begin { pNiceMatrix } }
  {
    \CodeAfter
      \tikz \draw (1 -| \int_use:N \c@jCol ) 
            -- ( \int_eval:n { \c@iRow + 1 } -| \int_use:N \c@jCol ) ;
    \end { pNiceMatrix }
  }
\ExplSyntaxOff
\makeatother



\newcommand{\Mataug}[1]{
  \begin{pNiceMatrixAug}
    #1
  \end{pNiceMatrixAug}
}

\newcommand{\Det}[1]{
  \begin{vmatrix}
    #1
  \end{vmatrix} 
}

\newcommand{\suite}[2][n\in \N]{\left( #2 \right )_{#1} }
\newcommand{\fl}[1]{\overrightarrow{#1}}
\newcommand{\infini}{\infty}
\newcommand{\vide}{\varnothing}
\newcommand{\oni}[1]{o_{n\to \infini}\left (#1\right )}
\newcommand{\Oni}[1]{O_{n\to \infini}\left (#1\right )}
\newcommand{\The}[2][]{\Theta_{#1}\left (#2\right )}


\def\eqni{\underset{{\small n\rightarrow\infty}}{\scalebox{1.5}[1]{$\sim$}}}
\newcommand{\eq}[1][x\to\infty]{  \underset{{\small #1}}{\ \scalebox{1.5}[1]{$\sim$} \ }  }
\newcommand{\tend}[1][x\to\infty]{  \underset{{\small #1}}{\longrightarrow}  }
\newcommand{\chemin}[1][]{  \underset{{\small #1}}{\curvearrowright}  }
\newcommand{\vers}[1]{\underset{{\small #1}}{\rightarrow} }
\newcommand{\dl}[1][n\to\infty]{\underset{\small #1}{=}   }
\def\limn{\lim_{n\to\infini}}



\newcommand{\exs}[1]{ \textit{Exemples: }\begin{enumerate}#1\end{enumerate} }



\newcommand{\multi}[2][2]{\begin{multicols}{#1}#2 \end{multicols}}
\newcommand{\enum}[1]{\begin{enumerate}#1\end{enumerate}}


\newcommand{\ens}[2]{ \left\{ \left.  #1  \middle |  \right.  #2 \right\} }
\newcommand{\ensp}[2]{ \left\{ #1 \; ; \; #2 \right\} }
\newcommand\ent[1] {\llbracket #1 \rrbracket}
\newcommand\entso[1] {\llbracket #1 \llbracket}
\newcommand{\pe}[1]{\left\lfloor\:  #1\: \right\rfloor } 
\newcommand{\ps}[2]{\left\langle \left . #1 \mid  #2 \right . \right\rangle}
\newcommand{\norme}[1]{\left \Vert #1 \right \Vert}
\newcommand {\fonc}[4] {
\begin{array}{rcl}
 {#1} & \rightarrow & {#2} \\
 {#3} & \mapsto     & {#4}
\end{array}}

\newcommand{\indenteBarre}[1]{
\begin{tcolorbox}[bottomrule=0pt,toprule=0pt,rightrule=0pt,leftrule=.2mm,arc=0pt,colback=white,left=5mm,frame empty,boxsep=0pt]
#1
\end{tcolorbox}
}
\def\absurde{\indenteBarre}

\newcommand{\prop}[1]{\begin{proposition}#1\end{proposition}}
\newcommand{\defin}[1]{\begin{definition}#1\end{definition}}
\newcommand{\exo}[2][]{\begin{Exercice}\textbf{#1}\\ #2\end{Exercice}}



%----------------------------------------------------
% --------------- environnements --------------------
%----------------------------------------------------




\newcommand{\cqfd}{\hfill $\square$\\}
\newenvironment{demo}{ \setcounter{equation}{0}\textit{Démonstration: } \small }{\cqfd}
\newenvironment{preuve}{ \setcounter{equation}{0}\textit{Preuve du lemme: } \small }{\cqfd}
\newcommand{\dem}[1]{\begin{demo}#1\end{demo}}

\theoremstyle{plain}
\newtheorem{theoreme}{Théorème} [section]
\newtheorem{definition}[theoreme]{Définition}
\newtheorem{definitions}[theoreme]{Définitions}
\newtheorem{exe}[theoreme]{Exemple}
%\theoremstyle{definition}
\newtheorem{proposition}[theoreme]{Proposition}
\newtheorem{lemme}[theoreme]{Lemme}

\newtheorem{corollaire}[theoreme]{Corollaire}
\newcommand{\cor}[1]{\begin{corollaire}#1\end{corollaire}}


\lstnewenvironment{python}[1][]{\lstset{language = python, #1}}{}
\lstnewenvironment{sql}[1][]{\lstset{language = sql, #1}}{}
\lstnewenvironment{html}[1][]{\lstset{language = html, #1}}{}
\def\|{\lstinline|} %|


\lstset{includerangemarker=false} %Ne pas afficher les marqueurs dans les morceaux de listing

% \newcommand\morceaupython[1][]{\leavevmode\lstinputlisting[ frame=single, frameround=tttt, rangeprefix=\#\#\#\ , rangesuffix=\ \#\#\#, #1]}
%\newcommand\morceaucaml[1][]{\leavevmode\lstinputlisting[ frame=single, frameround=tttt, rangeprefix=(*- , rangesuffix= -*), #1]}

% Version minted
\def\modePython{
  \lstset{language=python, rangeprefix=\#\#\#\ , rangesuffix=\ \#\#\#} % Je paramètre listings aussi
  \def\codePrefixe{\#\#\# }
  \def\codeSuffixe{\#\#\# }
  \def\langage{python}
  %\renewenvironment{lstlisting}{\begin{minted}{python}}{\end{minted}}
}
\def\modeCaml{
  \lstset{language=caml, rangeprefix=(*-\ , rangesuffix=\ -*)}
  \def\codePrefixe{(*- }
  \def\codeSuffixe{ -*)}
  \def\langage{ocaml}
  %\renewenvironment{lstlisting}{\begin{minted}{ocaml}}{\end{minted}}
}
\newcommand{\codeCorrige}[1]{
  \lstinputlisting[linerange=#1-fin]{\cheminCorrige}
}%\cheminCorrige doit être déf dans le .tex lui-même.

% Ne marche pas...
% \renewcommand{\codeCorrige}[1]{
%   \immediate\write18{minted_delim.py "\cheminCorrige" "\codePrefixe #1 \codeSuffixe" "\codePrefixe fin \codeSuffixe"}
%   \inputminted{\langage}{\cheminCorrige.snippet}
%   }

\def\annalesOption{/home/moi/enseignement/Informatique/annales/option/}
\def\CCP{\annalesOption CCP/}

